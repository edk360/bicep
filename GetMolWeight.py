#Author:            Robbin Bouwmeester (supervisors: Erik van Dijk & Sanne Abeln, feedback and server implementation: Bernd Brandt)
#Purpose:           Calculate the molecular weight of a protein based on a PDB file
#Date:              19-06-2014
#Python version:    2.7.5
#Packages:          -
#Bugs:              No bugs found...

#TODO maybe move to PDBStruc file

#Imports
from PDBStruc import PDBStructure

#Set the molecular weights of atoms
#From: http://physics.nist.gov/cgi-bin/Compositions/stand_alone.pl
molWeightAtoms = {"C":12.0,
            "H":1.0078250320710,
            "N":14.00307400486,
            "O":15.9949146195616,
            "S":31.9720710015
}

molWeightAA = {"A":71.09,
            "R":156.19,
            "D":114.11,
            "N":115.09,
            "C":103.15,
            "E":129.12,
            "Q":128.14,
            "G":57.05,
            "H":137.14,
            "I":113.16,
            "L":113.16,
            "K":128.17,
            "M":131.19,
            "F":147.18,
            "P":97.12,
            "S":87.08,
            "T":101.11,
            "W":186.12,
            "Y":163.18,
            "V":99.14
}

translateToOneAA = {"ALA":"A",
            "ARG":"R",
            "ASP":"D",
            "ASN":"N",
            "CYS":"C",
            "GLU":"E",
            "GLN":"Q",
            "GLY":"G",
            "HIS":"H",
            "ILE":"I",
            "LEU":"L",
            "LYS":"K",
            "MET":"M",
            "PHE":"F",
            "PRO":"P",
            "SER":"S",
            "THR":"T",
            "TRP":"W",
            "TYR":"Y",
            "VAL":"V"
}

"""Function that calculates the molecular weight, with the following parameters:
        PDB_ID          = define a "PDB identifier", could be any random identifier
        hdir            = optionally define a directory where the PDB and DSSP files are located, the name should correspond to the give PDB identifier without the extension! 
        PDB_file        = instead of defining a directory you can also define a location for the PDB file
        molweightDict   = optionally give a own defined dictionary with molecular weights
        useWeightAtoms  = boolean that indicates if we should use atoms instead of amino acids
    
    The function will return an integer or float (depends on the given dictionary).
"""

def getMolecularWeight(PDB_ID,hdir = "./",PDB_file = "",molweightDict={},useWeightsAtoms=False):
    #Is a dictionary given? Use that one, otherwise use our own dictionary
    if len(molweightDict.keys()) == 0: 
        if useWeightsAtoms: molweightDict = molWeightAtoms
        else: molweightDict = molWeightAA
    #What about isotopes? Maybe take the average atomic mass
    
    #Make a PDB object
    pdbStructure = PDBStructure(PDB_ID)
    #Starting molecular weight
    molecularWeight = 0.0
    
    #Try to read the PDB file, based on location directory or location of the file
    if len(PDB_file) > 0: 
        pdbStructure.readPDBfile(PDB_file)
    else:
        pdbStructure.readPDBfile(hdir+PDB_ID+".pdb")
    
    #Iterate over all chains
    for chain_num in  pdbStructure.chains.keys():
        #Iterate over all residues
        for res_num in pdbStructure.chains[chain_num].residues.keys():
            #Get the residues
            residue = translateToOneAA[pdbStructure.chains[chain_num].residues[res_num].aa]
            #Iterate over the atoms in the residue
            if useWeightsAtoms:
                #Get the atoms in the residue
                atoms = residue.atoms.keys()
                
                for atom in atoms:
                    #Add the atom to the total molecular weight
                    try:
                        molecularWeight += molweightDict[atom[:1]]
                    except KeyError:
                        sys.stderr.write("Atom (%s) was not defined in the atomic weight dictionary.\n" % (atom))
                        exit(-1)
            else:
                try:
                    molecularWeight += molweightDict[residue]
                except KeyError:
                    sys.stderr.write("Amino acid (%s) was not defined in the atomic weight dictionary.\n" % (residue))
                    exit(-1)

                   
    #Return the molecular weight
    return molecularWeight