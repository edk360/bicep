#Author:            Robbin Bouwmeester (supervisors: Erik van Dijk & Sanne Abeln, feedback and server implementation: Bernd Brandt)
#Purpose:           Wrapper for calculating the linear increase in heat capicity of folded proteins
#                   also plotting data and getting some statistics
#Date:              19-06-2014
#Python version:    2.7.5
#Packages:          Matplotlib V1.3.1
#Bugs:              No bugs found...

#Imports
from subprocess import Popen, PIPE
from os.path import isfile
import sys

"""Function that runs the program DSSP on a pdb file.

    Inputs:
        fname       - define the location of the pdb input file
        outputFname - define the location of the output file
        DSSP_PATH   - location of the DSSP binary
        **kwargs    - add any parameter you like, just like normal parameters but now in string like: paramx="16"
    
    outputs:
        -
        """
def runDSSP(fname,outputFname,DSSP_PATH,**kwargs): 
    #Make a dictionary for all parameters
    options = {}
    #Define the input and output file
    options['out'] = outputFname
    options['in'] = fname
    
    #Is the DSSP binary present?
    if not isfile(DSSP_PATH):
        sys.stderr.write("Could not locate the DSSP binary file\n")
        exit(-1)
        
    #Get the extra parameters
    options.update(**kwargs)
    
    #Make a command
    command = DSSP_PATH+" %(in)s %(out)s" % options
    
    #Add all the extra parameters, we have already defined the in and output -> skip these
    for option in options.keys():
        if option not in ('in','out'):
            command+=" -%s %s" % (option,options[option])
    
    #Logging...
    sys.stderr.write("<> DSSP\t%s\n" % (command))
    
    #Execute DSSP
    p = Popen(command,shell=True,stdout=PIPE,stderr=PIPE,close_fds=True)
    
    #Are there any errors from DSSP?
    errorMsg = ""
    crash = False
    for line in p.stderr:
                if line == "\n": continue
                if "dsspcmbi: not found" in line:
                        sys.stderr.write("Could not run DSSP because it is missing or not executable\nThe following error was generated:\n")
                        sys.stderr.write(line+"\n")
			sys.exit(-1)
		if "!!!" in line:
                        errorMsg += line
                if ":" in line:
                        crash = True
                        errorMsg += line
    sys.stderr.write(errorMsg+"\n")
    if crash:
                sys.stderr.write("\nCould not continue executing due to the error above\n")
                exit(-1)

    #Close some vars
    p.stderr.close()
    p.stdout.close()
    del(p.stdout)
