#!/usr/bin/python

##BERND: save as UNIX, add python call, add PATH to DSSPCMBI relative to wrapper
##Add \n after errors. Now we see e.g. "Error: Cannot open PDB file AccUnfold.data.bwbrandt$"

#Author:            Robbin Bouwmeester (supervisors: Erik van Dijk & Sanne Abeln, feedback and server implementation: Bernd Brandt)
#Purpose:           Wrapper for calculating the linear increase in heat capicity of folded proteins
#                   also plotting data and getting some statistics
#Date:              19-06-2014
#Python version:    2.7.5
#Packages:          Matplotlib V1.3.1
#Bugs:              No bugs found...

#Imports
import sys
import SurfaceHydrophobes
from GetMolWeight import getMolecularWeight
from runDSSP import runDSSP
from calculateHC import calculateFormulaHC
from os.path import isfile

#BERND: MPL DIR
import os
mplconfigdir = '/scratch/jobs/'
if mplconfigdir!=None: os.environ['MPLCONFIGDIR'] = mplconfigdir

##################################################################################################
#Make sure we log all the stderr, use standard logging library with some extension               #
#The code below (till "END COPY CODE") was taken from a blog by Ferry Boender with a GPL license:#
# http://www.electricmonk.nl/log/2011/08/14/redirect-stdout-and-stderr-to-a-logger-in-python/    #
##################################################################################################
import logging

class StreamToLogger(object):
   """
   Fake file-like stream object that redirects writes to a logger instance.
   """
   def __init__(self, logger, log_level=logging.INFO):
      self.logger = logger
      self.log_level = log_level
      self.linebuf = ''
 
   def write(self, buf):
      for line in buf.rstrip().splitlines():
         self.logger.log(self.log_level, line.rstrip())

def setLoggerToFile(outfile="out.log",append=True):
    if append == True: logmode = "a"
    else: logmode = "w"
    #Set some basic params for logging stderr
    logging.basicConfig(
       level=logging.DEBUG,
       format="%(asctime)s:%(levelname)s:%(name)s:%(message)s",
       filename=outfile,
       filemode=logmode
    )

    #Make a logger object for the stderr
    stderr_logger = logging.getLogger('STDERR')

    #Define an (write-) object where we write the log to
    sl = StreamToLogger(stderr_logger, logging.ERROR)

    #Set the stderr to the defined object
    sys.stderr = sl

#################
# END COPY CODE #
#################

def main():
    #The first argument should be the location of a PDB file
    try:
        pdbFile = sys.argv[1]
    except IndexError:
        sys.stderr.write("Could not load the pdb file because it was not specified.\n")
        sys.stderr.write("Please run this file by: \n\npython HydrophobicEffectWrapper <PDB file>\n\n")
        exit(-1)
    
    #Too many arguments?
    if len(sys.argv) > 2:
        sys.stderr.write("Got more than 1 argument, it will ignore the other arguments and continue!\n")
    
    #Does the PDB file exist?    
    if not isfile(pdbFile):
        sys.stderr.write("Could not find the input pdb file, will exit.\n")
        exit(-1)
    
    try:
        #Run DSSP
		#BERND: add rel path to dssp, relative to call location of called wrapper code, not rel to calling dir
		#BERND: somehow the latter is the case
		#BERND: removed output/ dir: this has to be created first and is not needed for server...
        runDSSP(pdbFile,"out.dssp","/opt/apps/heatcap/robbin/bin/dsspcmbi")
    except Exception,e:
        sys.stderr.write("Unable to run DSSP\n")
        sys.stderr.write("Following error message was generated:\n")
        sys.stderr.write(str(e)+"\n")
        exit(-1)
    
    try:
        #Get the hydrophobic surface area
		#BERND: removed output/ dir: this has to be created first and is not needed for server...
        surfaceStats = SurfaceHydrophobes.getHSA(pdbFile,PDB_file=pdbFile,DSSP_file="out.dssp")
        HSA = surfaceStats[5]
    except Exception,e:
        sys.stderr.write("Not able to get the hydrophobic surface area.\n")
        sys.stderr.write("Following error message was generated: \n")
        sys.stderr.write(str(e)+"\n")
        exit(-1)
    
    try:
        #Get the molecular weight
        mw = getMolecularWeight(pdbFile,PDB_file=pdbFile)
    except Exception,e:
        sys.stderr.write("Not able to get the molecular weight.\n")
        sys.stderr.write("Following error message was generated: \n")
        sys.stderr.write(str(e)+"\n")
        exit(-1)
    
    try:
        #Write a linear increase in heat capacity of a folded protein to a file
        print "Formula obtained: %s" % (calculateFormulaHC(HSA,mw,plotFile="out.png"))
    except Exception,e:
		#BERND: removed output/ dir: this has to be created first and is not needed for server...
        sys.stderr.write("Not able to calculate the linear increase or generate the plot,\n")
        sys.stderr.write("Following error message was generated: \n")
        sys.stderr.write(str(e)+"\n")
        exit(-1)

if __name__ == "__main__":
    setLoggerToFile()
    main()
