#Author:            Robbin Bouwmeester (supervisors: Erik van Dijk & Sanne Abeln, feedback and server implementation: Bernd Brandt)
#Purpose:           Wrapper for calculating the linear increase in heat capicity of folded proteins
#                   also plotting data and getting some statistics
#Date:              19-06-2014
#Python version:    2.7.5
#Packages:          MatplotLib V1.3.1
#Bugs:              No bugs found...

#BERND: MPL DIR
import os, os.path
mplconfigdir = '/scratch/jobs/'
if mplconfigdir!=None: os.environ['MPLCONFIGDIR'] = mplconfigdir

#BERND
import matplotlib
matplotlib.use('Agg')

#Imports
import numpy
from matplotlib import pyplot


"""Function that plots the linear increase in heat capacity of a folded protein.
        Inputs:
            HSA     - a float with the hydrophobic surface area in Angstroms
            mw      - the molecular weight
            makePlot- boolean, should we make a plot?
            plotFile- optionally define an output file
            show    - boolean, show the plot in a graphical window
            startT  - start temperature in Celsius to plot
            stopT   - stop temperature in Celsius to plot
        Outputs:
            formula - a string that contains a [FLOAT]*T+[FLOAT]
"""
#BERND: removed output/ dir
def calculateFormulaHC(HSA,mw,makePlot=True,plotFile="out.png",show=False,startT=-10,stopT=100,slopecoefficient=0.09003,interceptcoefficient=1.227):
    #Is the start temperature higher than stop?
    if startT >= stopT:
        sys.stderr.write("Cannot have a starting range that is lower than the stopping range\n")
        exit(-1)
    if makePlot == True and plotFile == "":
        sys.stderr.write("Please specify a name for the plot\n")
        exit(-1)
        
    #Define a range    
    x = numpy.linspace(startT,stopT,10)
    #Define the function
    y = HSA*slopecoefficient*x+interceptcoefficient*mw
    
    #Should we make a plot
    if makePlot == True: plotHeatCapacity(x,y,plotFile=plotFile,show=show,startT=startT,stopT=stopT)
    return "%s*T+%s" % (HSA*slopecoefficient,interceptcoefficient*mw)
    
"""Function that plots the linear increase in heat capacity of a folded protein.
        Inputs:
            x       - list of x-values (floats) to plot that should have the same size as y and consistent indexes
            y       - list of y-values (floats) to plot that should have the same size as x and consistent indexes
            plotFile- optionally define an output file
            show    - boolean, show the plot in a graphical window
            startT  - start temperature in Celsius to plot
            stopT   - stop temperature in Celsius to plot
        Outputs:
            -
"""
#TODO test if startT or stopT can be floats or must be integers
def plotHeatCapacity(x,y,plotFile="out.png",show=False,startT=-10,stopT=100,ticksteps=20):
    #Is the start temperature higher than stop?
    if startT >= stopT:
        sys.stderr.write("Cannot have a starting range that is lower than the stopping range\n")
        exit(-1)
    if len(x) != len(y):
        sys.stderr.write("Length of x and y coordinates must be the same if you want a plot\n")
        exit(-1)

    #Plot the function
    pyplot.plot(x,y)
    
    #Define axis labels
    pyplot.ylabel("Heat capacity ($J$ $K^{-1}$ $mol^{-1}$)")
    
    #Set the ticks and labels
    ticks = range(startT,stopT,ticksteps)
    
    #Set the labels it should use
    labels = numpy.arange(273.15+startT,stopT+273.15,ticksteps) 
    
    #Set the x-axis scale
    pyplot.xticks(ticks, labels)
    #Set the x-axis label
    pyplot.xlabel("Temperature ($K$)")
    
    #Save the figure
    try:
        pyplot.savefig(plotFile, bbox_inches='tight')
    except Exception,e:
        sys.stderr.write("Could not write the plot to a file\n")
        sys.stderr.write("Following error message was generated:\n")
        sys.stderr.write(str(e)+"\n")
        exit(-1)
    
    #Possibly show the figure
#    if show: 
#        try:
#            pyplot.show()
#        
#
#        except Exception,e:
#            sys.stderr.write("Could not show the plot on the screen")
#            sys.stderr.write("Following error message was generated: ")
#            sys.stderr.write(str(e))
#            exit(-1)
